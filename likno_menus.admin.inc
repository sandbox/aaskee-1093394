<?php
// $Id: likno_menus.admin.inc,v 1.0.4 2010/08/10 17:35:12 askesson Exp $

/**
 * @file likno_menus.admin.inc
 * Functions that are only called on the admin pages.
 */


function likno_menus_require() {

  $error_flag = "";
  $error = ""; // display error when the Superfish files in the likno_menus module are missing
               // AND when the Superfish module is not installed
  if (!module_exists('superfish')) {
    $error = "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_js/jquery.hoverIntent.minified.js</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/jquery.hoverIntent.minified.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_js/jquery.bgiframe.min.js</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/jquery.bgiframe.min.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_js/superfish.js</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/superfish.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_js/supersubs.js</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/supersubs.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_css/superfish.css</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/supersubs.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_css/superfish-navbar.css</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/supersubs.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
    $error .= "</li>";
    $error .= "<li><i>".drupal_get_path('module', 'likno_menus')."/superfish_css/superfish-vertical.css</i>";
    if (!file_exists(drupal_get_path('module', 'likno_menus') .'/superfish_js/supersubs.js')) {
      $error .= " <span style='color:red;'>(missing!)</span>";
      $error_flag = "true";
    }
  }

  if ($error_flag == "")
    return "";
  else
    return $error;
}


/**
 * Module settings form.
 */

function likno_menus_settings(&$form_state) {
  global $base_url, $base_path;


  $error = likno_menus_require();
  if ($error != "") {
    $form['requirements'] = array(
      '#type' => 'item',
      '#value' => t("<div align='center'><div align='left' style='border:1px solid #D9EAF5;padding:10px;width:70%'><p><b>Important!</b><br>This Drupal module also requires some additional files from the &quot;Superfish module&quot; to be installed. To do so, please ". l('download this zip file', 'http://www.likno.com/downldd/superfish_js_css_for_likno.zip') ." and extract it inside the &quot;/modules/likno_menus&quot; folder (the &quot;<em>superfish_js</em>&quot; and &quot;<em>superfish_css</em>&quot; folders will be added).<br><br>Eventually, you should have the following files installed:<ul>$error</ul>If any of the above files are missing, your &quot;Superfish-driven&quot; menus <strong>may not appear properly!</strong></p></div></div>"),
    );
  }
  else {
    $form['space'] = array(
      '#type' => 'item',
      '#value' => '<br>',
    );
  }

  // AWM online folder 
  $form['likno_menus_menu_folder'] = array(
    '#type' => 'textarea',
    '#title' => t('Online folder for menu files'),
    '#default_value' => variable_get('likno_menus_menu_folder', '/menu/'),
    '#cols' => 10,
    '#rows' => 1,
    '#description' => t('Every time you compile your local Likno-Menus project you should upload the compiled menu files at this folder: 
<i>'. $base_url .'<span id="likno_menus_the_path">' . variable_get('likno_menus_menu_folder', '/menu/') . '</span></i><br />(note: you need to create this online folder yourself)'),
  );
  $form['space0'. $likno_menus_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  $form['title0'. $likno_menus_t] = array(
    '#type' => 'item',
    '#value' => '<b>Available Likno-Menus:</b>',
  );

  //multiple menus
  for ($likno_menus_t=1; $likno_menus_t <= variable_get('LIKNO_MENUS_TOTAL_TABS', 1); $likno_menus_t++) {

    // AWM Main 
    $menu_name = variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t);
    $menufilename = $_SERVER['DOCUMENT_ROOT'] . $base_path . variable_get('likno_menus_menu_folder', '/menu/') . $menu_name .".js";
    $menufilenameexists = file_exists($menufilename);
    $likno_menus_correct_or_not = 0;

    if ($menufilenameexists && variable_get('likno_menus_menu_source_'. $likno_menus_t, 'superfish') == 'allwebmenus') {
      $menufile = fopen($menufilename, 'r');
      $mfile = fread($menufile, filesize($menufilename));
      $b_no = explode('awmLibraryBuild=', $mfile);
      $b_no = explode(';', $b_no[1]);
      $build_no = $b_no[0];
      $h_no = explode('awmHash=\'', $mfile);
      $h_no = explode('\'', $h_no[1]);
      $hash_no = $h_no[0];
      $params = "build=$build_no&plugin=drupal&hash=$hash_no&rand=". rand(1, 10000) ."&domain=". $base_url;
    }
    if (function_exists('curl_init') && $menufilenameexists && variable_get('likno_menus_menu_source_'. $likno_menus_t, 'superfish') == 'allwebmenus') {
      $likno_menus_tmp_geturl = likno_menus_geturl($hash_no, $params);
      if (strlen($likno_menus_tmp_geturl) > 2 ) {
        $likno_menus_correct_or_not = 1;
      }
    }

    $menu_name_source = (variable_get('likno_menus_menu_source_'. $likno_menus_t, 'superfish') == 'superfish')? t(" (Superfish)") : t(" (AllWebMenus)");
    $menu_name_title = variable_get('likno_menus_menu_active_'. $likno_menus_t, FALSE)? t($menu_name ." - Enabled". $menu_name_source) : t($menu_name);
    if ($likno_menus_t==1 && variable_get('likno_menus_menu_name_1', 'firsttime') == 'firsttime')
      $menu_name_title = t($menu_name ." - Enabled". $menu_name_source); // first time loading fix so that the 1st menu is enabled
    
    $form['likno_menus_menu'. $likno_menus_t] = array(
      '#type' => 'fieldset',
      '#title' => $menu_name_title,
      '#collapsible' => TRUE,
      //'#collapsed' => $likno_menus_t==0? FALSE : TRUE,
      '#collapsed' => TRUE,
    );

    // show warning
    if ($likno_menus_correct_or_not) {
      $form['likno_menus_menu'. $likno_menus_t]['likno_menus_warning_'. $likno_menus_t] = array(
        '#type' => 'item',
        '#value' => t("<table><tr><td style='vertical-align:top;width:30px;'><i style='text-decoration:underline;'>Note:</i></td><td>$likno_menus_tmp_geturl</td></tr></table>"),
      );
    }

    $likno_menus_main_def = $likno_menus_t==1? 1 : 0; // first time loading fix so that the 1st menu is enabled
    $likno_menus_indic_color = variable_get('likno_menus_menu_active_'. $likno_menus_t, FALSE)? 'green' : 'red';
    if ($likno_menus_t==1 && variable_get('likno_menus_menu_name_1', 'firsttime')=='firsttime') $likno_menus_indic_color = 'green'; // first time loading fix so that the 1st menu is enabled
    $likno_menus_indic_text = variable_get('likno_menus_menu_active_'. $likno_menus_t, FALSE)? t("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site") : t("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will NOT appear in your site");
    if ($likno_menus_t==1 && variable_get('likno_menus_menu_name_1', 'firsttime')=='firsttime') $likno_menus_indic_text = t("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site"); // first time loading fix so that the 1st menu is enabled
    $form['likno_menus_menu'. $likno_menus_t]['likno_menus_menu_active_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Show <i>'. variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t) .'</i> in site <span id="likno_menus_indicate_txt'. $likno_menus_t .'" class="likno_menus_indicate_txt" style="color:'. $likno_menus_indic_color .'">'. $likno_menus_indic_text .'</span>'),
      '#default_value' =>  variable_get('likno_menus_menu_active_'. $likno_menus_t, $likno_menus_main_def), //by default show the 1st menu? (YES)
      '#attributes' => array('class' => "likno_menus_menu_structure_enabling"),
    );

    $form['likno_menus_menu'. $likno_menus_t]['likno_menus_menu_name_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#title' => t('Menu name'),
      '#default_value' =>  variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t),
      '#cols' => 20,
      '#rows' => 1,
      '#description' => t('Please make sure that the "Menu name" value matches 
                        the value in the "Compiled Menu Name" property of the AllWebMenus project file 
                        (Tools > Project Properties > Folders). <a href="#" id="likno_menus_show_me'. $likno_menus_t .'">show me</a>'.
                        '<div style="margin-top: 20px; display: none; background-color: #FEFCF5; border: #E6DB55 solid 1px;" id="likno_menus_show_this'. $likno_menus_t .'">'.
                        '  <table>
                             <tr><td style="width: 600px; text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>More info</strong> - <a href="#" id="likno_menus_show_me_close'. $likno_menus_t .'">close</a></td></tr>
                             <tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
                               <img src="'. $base_url .'/'. drupal_get_path('module', 'likno_menus') .'/more_info.jpg" width="486" height="560" alt="More info" title="More info"/>
                             </td></tr>
                             <tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
                               <a href="#" id="likno_menus_show_me_close'. $likno_menus_t .'">close</a>
                             </td></tr>
                           </table>
                         </div>'),
    );


    $likno_menus_existing_menus_all=menu_get_names();
    $likno_menus_existing_menus=array();
    // REMOVE MENUS THAT DO NOT HANDLE NODES! (navigation menu)
    while ($item = current($likno_menus_existing_menus_all)) {
      if ($item != 'navigation') $likno_menus_existing_menus[] = array_shift($likno_menus_existing_menus_all);
      else array_shift($likno_menus_existing_menus_all);
    }

    $form['likno_menus_menu'. $likno_menus_t]['likno_menus_new_menu_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('<strong>Get the items and structure from one of the existing Drupal menus:</strong>'),
      '#default_value' =>  variable_get('likno_menus_new_menu_'. $likno_menus_t, 0),
      '#attributes' => array('class' => "likno_menus_menu_newold_". $likno_menus_t),
    );
    $likno_menus_disabled = FALSE;
    if (!variable_get('likno_menus_new_menu_'. $likno_menus_t, 0))
      $likno_menus_disabled = TRUE;
    if ($likno_menus_existing_menus == NULL)
      $likno_menus_existing_menus[] = t("There are no menus currently defined in your Drupal site");
    $form['likno_menus_menu'. $likno_menus_t]['likno_menus_items_from_menu_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#options' => $likno_menus_existing_menus,
      '#default_value' =>  variable_get('likno_menus_items_from_menu_'. $likno_menus_t, 0),
      '#disabled' => $likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_newold_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['title1'. $likno_menus_t] = array(
      '#type' => 'item',
      '#value' => t('<strong>...OR create a new menu by setting the Menu Structure below:</strong>'),
    );


    // Menu Structure:
    //home
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t] = array(
      '#type' => 'fieldset',
      '#title' => t('Menu Structure'),
      '#collapsible' => TRUE,
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_include_home_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('"Home"'),
      '#default_value' =>  variable_get('likno_menus_include_home_'. $likno_menus_t, 1),
      '#description' => t('A "Home" item that opens the site\'s Home Page.'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['space1'. $likno_menus_t] = array(
      '#type' => 'item',
      '#value' => '<br>',
    );
    //pages
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_pages_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Pages'),
      '#default_value' =>  variable_get('likno_menus_pages_'. $likno_menus_t, 1),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_excluded_pages_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#title' => t('<span class="likno_menus_showAll">Show all Pages</span> <span class="likno_menus_showExcept">except the following</span>'),
      '#cols' => 20,
      '#rows' => 1,
      '#default_value' =>  variable_get('likno_menus_excluded_pages_'. $likno_menus_t, ''),
      '#description' => t('Node IDs that correspond to pages, separated by commas (their sub-pages will also be excluded). Example: 34, 59, 140'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_pages_ms_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#options' => array(
        'main' => t('Show Pages as Main Menu items'),
        'sub' => t('Group Pages under a Main Menu Item with name:'),
      ),
      '#default_value' =>  variable_get('likno_menus_pages_ms_'. $likno_menus_t, 'main'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_pages_name_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      //'#title' => t('PagesName'),
      '#cols' => 10,
      '#rows' => 1,
      '#default_value' => variable_get('likno_menus_pages_name_'. $likno_menus_t, 'Pages'),
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );

    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['space2'. $likno_menus_t] = array(
      '#type' => 'item',
      '#value' => t('<br>'),
    );
    //stories
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_stories_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Stories'),
      '#default_value' =>  variable_get('likno_menus_stories_'. $likno_menus_t, 0),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_excluded_stories_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#title' => t('<span class="likno_menus_showAll">Show the following Stories</span>'),
      '#cols' => 20,
      '#rows' => 1,
      '#default_value' =>  variable_get('likno_menus_excluded_stories_'. $likno_menus_t, ''),
      '#description' => t('Node IDs that correspond to stories, separated by commas. Example: 34, 59, 140'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_stories_ms_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#options' => array(
        'main' => t('Show Stories as Main Menu items'),
        'sub' => t('Group Stories under a Main Menu Item with name:'),
      ),
      '#default_value' =>  variable_get('likno_menus_stories_ms_'. $likno_menus_t, 'sub'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_stories_name_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#cols' => 10,
      '#rows' => 1,
      '#default_value' => variable_get('likno_menus_stories_name_'. $likno_menus_t, 'Stories'),
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );

    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['space3'. $likno_menus_t] = array(
      '#type' => 'item',
      '#value' => '<br>',
    );
    //taxonomy
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Taxonomies (Categories)'),
      '#default_value' =>  variable_get('likno_menus_taxonomies_'. $likno_menus_t, 0),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_excluded_taxonomies_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#title' => t('<span class="likno_menus_showAll">Show all Taxonomy terms</span> <span class="likno_menus_showExcept">except the following</span>'),
      '#cols' => 20,
      '#rows' => 1,
      '#default_value' =>  variable_get('likno_menus_excluded_taxonomies_'. $likno_menus_t, ''),
      '#description' => t('Node IDs that correspond to taxonomy terms, separated by commas (their sub-terms will also be excluded). Example: 34, 59, 140'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    /*$form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_with_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Only show Taxonomy Terms that contain posts'),
      '#default_value' =>  variable_get('likno_menus_taxonomies_with_'. $likno_menus_t, 0),
      //'#description' => t('A "Home" item that opens the site\'s Home Page.'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );*/
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_vocab_taxonomies_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Group Taxonomy terms by Vocabularies'),
      '#default_value' =>  variable_get('likno_menus_vocab_taxonomies_'. $likno_menus_t, 0),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_ms_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#options' => array(
        'main' => t('Show Taxonomies as Main Menu items'),
        'sub' => t('Group Taxonomies under a Main Menu Item with name:'),
      ),
      '#default_value' =>  variable_get('likno_menus_taxonomies_ms_'. $likno_menus_t, 'sub'),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_name_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      '#cols' => 10,
      '#rows' => 1,
      '#default_value' => variable_get('likno_menus_taxonomies_name_'. $likno_menus_t, 'Taxonomies'),
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_newest_'. $likno_menus_t] = array(
      '#type' => 'checkbox',
      '#title' => t('Also show the latest posts of each Category as its submenu items - number of items:&nbsp;&nbsp;(up to)&nbsp;&nbsp;'),
      '#default_value' =>  variable_get('likno_menus_taxonomies_newest_'. $likno_menus_t, 0),
      '#disabled' => !$likno_menus_disabled,
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );
    $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['likno_menus_taxonomies_newest_num_'. $likno_menus_t] = array(
      '#type' => 'textarea',
      //'#title' => t('Number of pages/stories'),
      '#cols' => 1,
      '#rows' => 1,
      '#description' => t('Value must be<br/>between 1 and 50.'),
      '#default_value' => variable_get('likno_menus_taxonomies_newest_num_'. $likno_menus_t, 5),
      '#attributes' => array('class' => "likno_menus_menu_structure_". $likno_menus_t),
    );

    // Menu Type:
    $form['likno_menus_menu'. $likno_menus_t]['menu_type'. $likno_menus_t] = array(
      '#type' => 'fieldset',
      '#title' => t('Menu Type'),
      '#collapsible' => TRUE,
    );

    $form['likno_menus_menu'. $likno_menus_t]['menu_type'. $likno_menus_t]['likno_menus_menu_source_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#title' => t('Please select whether you want your menu to be generated using the AllWebMenus software or the Superfish script'),
      '#options' => array(
        'allwebmenus' => t('Menu will be created by AllWebMenus.'),
        'superfish' => t('Menu will be created by Superfish. (Block-specific Superfish settings can be found at <a href="@block-page">admin/build/block</a>) <a style="font-style:italic;" href="#" id="sf_likno_menus_show_me'. $likno_menus_t .'">more info</a>'.
                       '<div style="margin-top: 20px; display: none; background-color: #FEFCF5; border: #E6DB55 solid 1px;" id="sf_likno_menus_show_this'. $likno_menus_t .'">'.
                       '  <table>
                            <tr><td style="width: 600px; text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>More info</strong> - <a href="#" id="sf_likno_menus_show_me_close'. $likno_menus_t .'">close</a></td></tr>
                              <tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
                                <p>For each menu generated by this module a new "block" is created. For menus created by the Superfish script this block must contain all the necessary information about the appearance and the behavior of the items and the menu. <a href="@block-page">Here</a> you will find all the blocks of your site; in order to configure this specific menu\'s settings, you have to configure the block who\'s name is the name of this menu.</p>
                              </td></tr>
                              <tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
                                <a href="#" id="sf_likno_menus_show_me_close'. $likno_menus_t .'">close</a>
                              </td></tr>
                            </table>
                          </div>', array('@block-page' => url('admin/build/block'))),
      ),
      '#default_value' =>  variable_get('likno_menus_menu_source_'. $likno_menus_t, 'superfish'),
      '#attributes' => array('class' => "awm-admin_block-tooltip"),
    );

    $likno_menus_Dynamic_show = (variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Dynamic') == 'Dynamic')? 'block' : 'none';
    $likno_menus_Mixed_show = (variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Dynamic') == 'Mixed')? 'block' : 'none';
    $likno_menus_Static_show = (variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Dynamic') == 'Static')? 'block' : 'none';
    $form['likno_menus_menu'. $likno_menus_t]['menu_type'. $likno_menus_t]['likno_menus_menu_type_'. $likno_menus_t] = array(
      '#type' => 'radios',
      '#title' => t('Please select how you want your menu to behave (only for menus created by AllWebMenus)'),
      '#options' => array(
        'Dynamic' => t('"Dynamic" Menu Type'.
                     '<div style="float: right;  width: 500px; display:'. $likno_menus_Dynamic_show .';" class="likno_menus_itemInfo" id="likno_menus_menu_type_info_'. $likno_menus_t .'_Dynamic">
                        <p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Dynamic Type".</p>
                        <p>This means that the menu items in AllWebMenus will only be used for preview/styling purposes.</p>
                        <p>In your actual blog these items will be ignored and the menu will be populated "dynamically" based on the plugin settings.</p>
                        <p>The styles in AllWebMenus Style Editor will be used to form the actual menu items.</p>
                      </div>'),
      'Mixed' => t('"Mixed" Menu Type'.
                   '<div style="float: right;  width: 500px; display: '. $likno_menus_Mixed_show .';" class="likno_menus_itemInfo" id="likno_menus_menu_type_info_'. $likno_menus_t .'_Mixed">
                      <p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Mixed Type".</p>
                      <p>This means that your menu will contain both the items you create within AllWebMenus ("static") and the items you import from Drupal ("dynamic").</p>
                      <p>The imported Drupal items will use the styles of the AllWebMenus Style Editor but their actual content will be populated "dynamically" based on the plugin settings.</p>
                      <p>The static items you create within AllWebMenus will be shown as is.</p>
                    </div>'),
      'Static' => t('"Static" Menu Type'.
                    '<div style="float: right;  width: 500px; display: '. $likno_menus_Static_show .';" class="likno_menus_itemInfo" id="likno_menus_menu_type_info_'. $likno_menus_t .'_Static">
                       <p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Static Type".</p>
                       <p>Your menu will be edited (addition/removal/customization of items) within AllWebMenus only.</p>
                       <p>Any changes on your online blog will not affect its items until you perform the "Save settings & Generate Menu Structure Code" action and <strong>re-import</strong> to AllWebMenus.</p>
                       <p>This allows for maximum customization, as your online menu will show all items and styles customized within AllWebMenus.</p>
                     </div>'),
      ),
      '#default_value' =>  variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Dynamic'),
      '#description' => t('<br/><p style="width: 140px; text-align: center;"> A <strong>Superfish</strong> menu\'s type is always "Dynamic".</p>'),
    );

 
  }

  $form['likno_menus_superfish_hid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable hoverIntent detection. (<strong>only for menus created by Superfish</strong>)'),
    '#description' => t('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="@hover-intent" target="_blank">What is hoverIntent?</a></i>', array('@hover-intent' => url('http://cherne.net/brian/resources/jquery.hoverIntent.html'))),
    '#default_value' => variable_get('likno_menus_superfish_hid', 1),
  );
  $form['likno_menus_menu'. $likno_menus_t]['menu_structure'. $likno_menus_t]['space3'. $likno_menus_t] = array(
    '#type' => 'item',
    '#value' => t('<i style="color:red;">Note!</i> Modify the appearance and behavior of a <strong>Superfish</strong> menu by configuring its Block settings at <a href="@block-page">admin/build/block</a>.</li></ul>', array('@block-page' => url('admin/build/block'))),
  );
  $return = likno_menus_system_settings_form($form);

return $return;
}


function likno_menus_system_settings_form($form) {
  $form['buttons']['gener_submit'] = array('#type' => 'submit', '#value' => t('Generate menu structure code and save configuration'), '#submit' => array('likno_menus_system_settings_form_submit') );
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
//  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults'), '#submit' => array('likno_menus_system_settings_form_submit') );
  $form['buttons']['tip'] = array(
    '#type' => 'item',
    '#value' => t('<p style="padding: 5px; color:#494949; border: 1px solid #494949; background-color: #DDD;"><strong>This button will reset the values of <span style="color:red;">ALL</span> the menus</strong></p>'),
    '#prefix' => t('<div id="awm-reset-tooltip" style="display:none;float:right; padding-right:150px; position:relative; top:-100px; width:190px;">'),
    '#suffix' => t('</div>'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  $form['#submit'][] = 'system_settings_form_submit';
  $form['#theme'] = 'system_settings_form';
  return $form;
}

function likno_menus_system_settings_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);

  $lkn_exclude = "-1";
  $lkn_include = -1;
  $likno_menus_xml_message = "";
  $likno_menus_xml_out = "";

  foreach ($form_state['values'] as $key => $value) {
    if (!strcmp(substr($key, 0, strlen($key)-1), "likno_menus_menu_active_") && $value==0) {
      $lkn_include++;
      $lkn_exclude = substr($key, -1);
    }
    elseif (!strcmp(substr($key, 0, strlen($key)-1), "likno_menus_menu_active_")) {
      $lkn_include++;
    }

    if ($op == t('Reset to defaults')) {
      variable_del($key);
      variable_set('likno_menus_menu_active_1', TRUE); // first menu is by default enabled
    }
    else {
      if (is_array($value) && isset($form_state['values']['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      variable_set($key, $value);
    }
  }
  if ($op == t('Reset to defaults')) {
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    $likno_menus_xml_message = likno_menus_create_xml();
    drupal_set_message(t($likno_menus_xml_message .'<br>'));
    drupal_set_message(t('The configuration options have been saved.'));
  }

  cache_clear_all();
  drupal_rebuild_theme_registry();
}

/**
* Basic function that generates the XML code for the menu,
* along with all the necessary parts to display it to the user
*/
function likno_menus_create_xml() {

  global $base_url;
  $likno_menus_ic = 1000;

  $likno_menus_xml_out = "Menu Structure Code for each menu:<ul><span>&nbsp;</span>";
  for ($likno_menus_t=1; $likno_menus_t<=variable_get('LIKNO_MENUS_TOTAL_TABS', 1); $likno_menus_t++) {
    if (!variable_get('likno_menus_menu_active_'. $likno_menus_t, '')) { // if menu is enabled 
      $likno_menus_xml_out .= "<li class='awmToggleHeadNot'>". variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t) ." - NOT enabled</li>";

    }
    elseif (variable_get('likno_menus_menu_source_'. $likno_menus_t, 'superfish') == 'superfish') {
      $likno_menus_xml_out .= "<li class='awmToggleHeadNot'>". variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t) ." - Superfish Menu (Block-specific menu settings can be found at ". l('admin/build/block', 'admin/build/block') .")</li>";

    }
    else {

      $likno_menus_xml_out .= "<li class='awmToggleHead'><a>".
                      variable_get('likno_menus_menu_name_'. $likno_menus_t, 'menu'. $likno_menus_t) ." - AllWebMenus Menu (Click here to get its Menu Structure Code)</a></li>".
                      "<div class='awmToggleBody'>".
                      "<textarea cols='89' rows='10' id='loginfo' name='loginfo'>";
      $likno_menus_xml_out .= "<?xml version='1.0' encoding='UTF-8'?>\n<mainmenu>"; //xml start 

      // xml menu type
      if ((variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Static') == 'Dynamic')) $likno_menus_xml_out .= "<menutype>Dynamic</menutype>"; 
      if ((variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Static') == 'Mixed')) $likno_menus_xml_out .= "<menutype>Mixed</menutype>"; 
      if ((variable_get('likno_menus_menu_type_'. $likno_menus_t, 'Static') == 'Static')) $likno_menus_xml_out .= "<menutype>Static</menutype>"; 

      $likno_menus_xml_out .= likno_menus_get_items($likno_menus_t, $likno_menus_parentgroup, $likno_menus_ic, TRUE);

      $likno_menus_xml_out .= "</mainmenu>"; //xml end 
      $likno_menus_xml_out .= "</textarea>";

      $likno_menus_xml_out .= '<div style="padding-left: 80px; width: 600px; text-align: left; padding-top: 10px; padding-bottom: 10px;">'.
                      '- Press <strong>Ctrl+C</strong> to copy the above code'.
                      '<br>- Switch to the AllWebMenus desktop application'.
                      '<br>- Open the <i>"Add-ins -> Drupal Menu -> Import/Update Menu Structure from Drupal"</i> form'.
                      '<br>- Paste the above copied "Menu Structure Code"'.
                      '<br>- Configure further your menu (styles, etc.) through the AllWebMenus properties'.
                      '<br>- Compile your menu from "Add-ins -> Drupal Menu -> Compile Drupal Menu"'.
                      '<br>- Make sure that you upload all compiled files in the <strong>'. $base_url . variable_get('likno_menus_menu_folder', 1) .'</strong> directory on your server'.
                      '</div>';
      $likno_menus_xml_out .= "</div>";
    }
  }
  $likno_menus_xml_out .= "</ul>";
  return $likno_menus_xml_out;
}

