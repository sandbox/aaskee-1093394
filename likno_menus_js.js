// $Id: likno_menus_js.js,v 1.0.4 2010/08/10 16:54:13 askesson Exp $

/**
* @file likno_menus_js.js
* The JavaScript code necessary for the behavior of the admin page
*/

//toggle
Drupal.behaviors.togg = function (context) {

  // BEGIN colors and styling...
  $("#edit-reset").hover(
    function() {
      $("#awm-reset-tooltip").show();
    },
    function() {
      $("#awm-reset-tooltip").hide();
    });

  $(".likno_menus_showAll").css('color', '#009900');
  $(".likno_menus_showExcept").css('color', '#990000');

  $("div[id^=edit-likno-menus-items-from-menu-]").each(function() {
    $(this).css('padding-left', '100px');
  });

  $("div[id^=edit-likno-menus-pages-name-]").each(function() {
    $(this).css('padding-left', '100px');
    $(this).css('padding-right', '500px');
    //$(this).css('position', 'relative');
    //$(this).css('top', '-30px');
  });
  $("div[id^=edit-likno-menus-stories-name-]").each(function() {
    $(this).css('padding-left', '100px');
    $(this).css('padding-right', '500px');
  });
  $("div[id^=edit-likno-menus-taxonomies-name-]").each(function() {
    $(this).css('padding-left', '100px');
    $(this).css('padding-right', '500px');
  });

  $("div[id^=edit-likno-menus-taxonomies-newest-]").each(function() {
    $(this).css('float', 'left');
  });
  // END colors and styling...

  // BEGIN RESULTS IN "GENERATE STRUCTURE CODE"
  //hide the all of the element with class msg_body
  $(".awmToggleBody").hide();
  //toggle the componenet with class awmToggleBody   
  $(".awmToggleHead").click(function() {
    if ($(this).next(".awmToggleBody").css('display') == 'block') {
      $(".awmToggleBody").slideUp(600); 
    } else {
      $(".awmToggleBody").slideUp(600); 
      $(this).next(".awmToggleBody").slideToggle(600);
    }
  });
  //add css 
  $(".awmToggleHead").each(function() {
    $(this).css("color", "#027AC6");
    //$(this).css("text-decoration", "underline");
    $(this).css("cursor", "pointer");
  });
  // END RESULTS IN "GENERATE STRUCTURE CODE"


  // BEGIN SHOW HIDE OF MORE INFO PIC
  //onclick, show the div
  $("*[id^=likno_menus_show_me]").each(function() {
    $(this).click(function() {
      $(this).next("*[id^=likno_menus_show_this]").slideDown();
      return false;
    });
  });
  $("*[id^=likno_menus_show_me_close]").each(function() {
    $(this).click(function() {
      $(this).parents("*[id^=likno_menus_show_this]").slideUp();
      return false;
    });
  });
  // END SHOW HIDE OF MORE INFO PIC

  // BEGIN SHOW HIDE OF MORE INFO for admin/block (superfish)
  //onclick, show the div
  $("*[id^=sf_likno_menus_show_me]").each(function() {
    $(this).click(function() {
      // IE? OPERA?  (SAFARI==CHROME??)
      if(!$.browser.safari)	$(this).parent().next("*[id^=sf_likno_menus_show_this]").slideDown(); // parents - link located into for <label>
      else			$(this).next("*[id^=sf_likno_menus_show_this]").slideDown();
      return false;
    });
  });
  $("*[id^=sf_likno_menus_show_me_close]").each(function() {
    $(this).click(function() {
      $(this).parents("*[id^=sf_likno_menus_show_this]").slideUp();
      return false;
    });
  });
  // END SHOW HIDE OF MORE INFO for admin/block (superfish)

  // BEGIN FORM ENABLING/DISABLING (existing or new menu)
  $(".likno_menus_menu_structure_enabling").each(function() {
    $(this).change(function() {
      if ($(this).nextAll('.likno_menus_indicate_txt').css('color') == 'green') {
        $(this).nextAll('.likno_menus_indicate_txt').css('color', 'red').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will NOT appear in your site');
      } else {
        $(this).nextAll('.likno_menus_indicate_txt').css('color', 'green').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site');
      }
    });
  });


  $("*[name=likno_menus_new_menu_1]").change(function() {
    if ($("*[name=likno_menus_items_from_menu_1]").attr("disabled") || $("*[name=likno_menus_items_from_menu_0]").attr("disabled") == "true") {
      $("*[name=likno_menus_items_from_menu_1]").attr("disabled", false);
      $(".likno_menus_menu_structure_1").attr("disabled", true);
    } else {
      $("*[name=likno_menus_items_from_menu_1]").attr("disabled", true);
      $(".likno_menus_menu_structure_1").attr("disabled", false);
    }
  });
  $("*[name=likno_menus_new_menu_2]").change(function() {
    if ($("*[name=likno_menus_items_from_menu_2]").attr("disabled") || $("*[name=likno_menus_items_from_menu_0]").attr("disabled") == "true") {
      $("*[name=likno_menus_items_from_menu_2]").attr("disabled", false);
      $(".likno_menus_menu_structure_2").attr("disabled", true);
    } else {
      $("*[name=likno_menus_items_from_menu_2]").attr("disabled", true);
      $(".likno_menus_menu_structure_2").attr("disabled", false);
    }
  });
  $("*[name=likno_menus_new_menu_3]").change(function() {
    if ($("*[name=likno_menus_items_from_menu_3]").attr("disabled") || $("*[name=likno_menus_items_from_menu_0]").attr("disabled") == "true") {
      $("*[name=likno_menus_items_from_menu_3]").attr("disabled", false);
      $(".likno_menus_menu_structure_3").attr("disabled", true);
    } else {
      $("*[name=likno_menus_items_from_menu_3]").attr("disabled", true);
      $(".likno_menus_menu_structure_3").attr("disabled", false);
    }
  });
  $("*[name=likno_menus_new_menu_4]").change(function() {
    if ($("*[name=likno_menus_items_from_menu_4]").attr("disabled") || $("*[name=likno_menus_items_from_menu_0]").attr("disabled") == "true") {
      $("*[name=likno_menus_items_from_menu_4]").attr("disabled", false);
      $(".likno_menus_menu_structure_4").attr("disabled", true);
    } else {
      $("*[name=likno_menus_items_from_menu_4]").attr("disabled", true);
      $(".likno_menus_menu_structure_4").attr("disabled", false);
    }
  });
  $("*[name=likno_menus_new_menu_5]").change(function() {
    if ($("*[name=likno_menus_items_from_menu_5]").attr("disabled") || $("*[name=likno_menus_items_from_menu_5]").attr("disabled") == "true") {
      $("*[name=likno_menus_items_from_menu_5]").attr("disabled", false);
      $(".likno_menus_menu_structure_5").attr("disabled", true);
    } else {
      $("*[name=likno_menus_items_from_menu_5]").attr("disabled", true);
      $(".likno_menus_menu_structure_5").attr("disabled", false);
    }
  });
  // END FORM ENABLING/DISABLING (existing or new menu)


  // BEGIN FORM TYPE INFO
  $("input[id^=edit-likno-menus-menu-type]").each(function() {
    $(this).change(function() {
      //which of the menus are we editing?
      likno_menus_tohide_id = $(this).attr('name').substr($(this).attr('name').length -1, $(this).attr('name').length);
      // hide all other infos of the same menu
      $("div[id^=likno_menus_menu_type_info_"+likno_menus_tohide_id+"]").each(function() {
        $(this).css('display', 'none');
      });

      // IE? OPERA?	(SAFARI==CHROME??)
      if($.browser.safari)	likno_menus_toshow = $(this).next();
      else			likno_menus_toshow = $(this).parent().next();
      if (likno_menus_toshow.css('display') == 'block') {
        likno_menus_toshow.css('display', 'none')
      } else {
        likno_menus_toshow.css('display', 'block')
      }
    });
  });
  // END FORM TYPE INFO

 };

